import AssemblyKeys._
import play.PlayImport.PlayKeys._
import com.typesafe.sbt.SbtSite.site
import com.typesafe.sbt.site.SphinxSupport._

name := "recording-server"

scalaVersion := "2.11.6"

version := "1.14.1"

lazy val root = (project in file(".")).enablePlugins(play.PlayScala)

libraryDependencies ++= Seq(jdbc,
                            anorm,
                            cache,
                            ws,
                            "xivo" %% "play-authentication" % "1.9.1",
                            "postgresql" % "postgresql" % "9.1-901.jdbc4",
                            "org.dbunit" % "dbunit" % "2.4.7",
                            "org.mockito" % "mockito-all" % "1.9.5",
                            "com.unboundid" % "unboundid-ldapsdk" % "2.3.6" % "test")

assemblySettings

mergeStrategy in assembly <<= (mergeStrategy in assembly) {
  (old) => {
    case PathList("application.conf") => MergeStrategy.discard
    case PathList("logback.xml") => MergeStrategy.discard
    case PathList("org", "apache", "commons", xs@_*) => MergeStrategy.first
    case PathList("play", xs@_*) => MergeStrategy.first
    case x => old(x)
  }
}

jarName in assembly := "recording-server.jar"

artifactName in playPackageAssets := {(_, _, _) => "recording-server-assets.jar"}

fullClasspath in assembly += file("target/recording-server-assets.jar")

test in assembly := {}

resolvers += "Local Maven Repository" at "file://"+Path.userHome.absolutePath+"/.m2/repository"

parallelExecution in Test := false

publishArtifact in (Compile, packageDoc) := false

test <<= (test in Test) dependsOn playPackageAssets

site.settings

site.sphinxSupport()

enableOutput in generatePdf in Sphinx := true

maintainer in Docker := "Jean Aunis <jaunis@avencall.com>"

dockerBaseImage := "java:openjdk-8u66-jdk"

dockerRepository := Some("xivoxc")

dockerExposedPorts := Seq(9000)

dockerEntrypoint := Seq("bin/recording-server-docker")

dockerExposedVolumes := Seq("/var/log")
