package helpers

trait DbTest {
  def testConfig = Map(
    "db.default.url" -> "jdbc:postgresql://localhost/asterisktest",
    "db.default.user" -> "asterisk",
    "db.default.password" -> "asterisk",
    "db.stats.url" -> "jdbc:postgresql://localhost/asterisktest",
    "db.stats.user" -> "asterisk",
    "db.stats.password" -> "asterisk",
    "application.secret" -> "test-secret",
    "evolutionplugin" -> "disabled",
    "audio.folder" -> "/tmp/sounds",
    "audio.extensions" -> List("wav"))
}
