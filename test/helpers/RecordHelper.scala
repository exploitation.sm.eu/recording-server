package helpers

import java.sql.Connection
import java.util.Date
import scala.util.Random
import anorm.SQL
import models.CallDirection
import models.CallDirection.CallDirection
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

object RecordHelper {
  val dateFormat = "yyyy-MM-dd HH:mm:ss"
  val formatter = DateTimeFormat.forPattern(dateFormat)
  val random = new Random()

  def insertIncall(number: String)(implicit c: Connection): Option[Long] = {
    val incallId = random.nextInt()
    SQL("INSERT INTO extensions(exten, type, typeval) VALUES ({number}, 'incall', {incallId})").on('number -> number, 'incallId -> incallId).executeUpdate()
    return Some(incallId)
  }

  def insertCallData(uniqueId: String, start: Date, end: Option[Date], srcNum: Option[String] = Some("0312"), dstNum: Option[String] = Some("1000"),
                     direction: CallDirection=CallDirection.Incoming, srcAgent:Option[String]=None, dstAgent: Option[String]=None, srcInterface: Option[String]=None)
                    (implicit connection: Connection): Option[Long] =
    SQL(s"""INSERT INTO call_data(uniqueid, src_num, dst_num, start_time, end_time, call_direction, src_agent, dst_agent, src_interface) VALUES
          ({uniqueId}, {srcNum}, {dstNum}, {start}, {end}, {direction}::call_direction_type, {srcAgent}, {dstAgent}, {srcInterface})""")
        .on('uniqueId -> uniqueId, 'srcNum -> srcNum, 'dstNum -> dstNum, 'start -> start, 'end -> end, 'direction -> direction.toString,
      'srcAgent -> srcAgent, 'dstAgent -> dstAgent, 'srcInterface -> srcInterface).executeInsert()

  def insertRecord(uniqueId: String, globalCallid: String, start: DateTime, end: Option[DateTime], srcNum: Option[String], dstNum: Option[String],
                   data: Map[String, String]=Map(), direction: CallDirection=CallDirection.Incoming,
                   srcAgent: Option[String]=None, dstAgent: Option[String]=None)
                  (implicit connection: Connection) {
    val theEnd = end.map(d => d.toDate)
    val cdId = insertCallData(uniqueId, start.toDate, theEnd, srcNum, dstNum, direction, srcAgent, dstAgent)
    insertAttachedData(cdId.get, "recording", globalCallid)
    for((key, value) <- data)
      insertAttachedData(cdId.get, key, value)
  }

  def insertAttachedData(callDataId: Long, key: String, value: String)(implicit connection: Connection) =
    SQL("INSERT INTO attached_data(id_call_data, key, value) VALUES ({callDataId}, {key}, {value})")
      .on('callDataId -> callDataId, 'key -> key, 'value -> value).executeInsert()

  def insertCallOnQueue(linkedId: String, agent: String, queue: String)(implicit connection: Connection) {
    SQL(s"INSERT INTO call_on_queue(callid, time, status, agent_num, queue_ref) VALUES('$linkedId', now(), 'answered', '$agent', '$queue')").executeUpdate
  }

  def insertAgentGroup(name: String)(implicit c: Connection): Option[Long] = SQL("INSERT INTO agentgroup(name) VALUES ({name})").on('name -> name).executeInsert()

  def insertAgent(firstName: String, lastName: String, number: String, groupId: Int)(implicit c: Connection): Option[Long] =
    SQL("INSERT INTO agentfeatures(firstname, lastname, number, numgroup) VALUES ({firstname}, {lastname}, {number}, {groupId})").on(
    'firstname -> firstName, 'lastname -> lastName, 'number -> number, 'groupId -> groupId).executeInsert()

  def insertQueue(name: String, number: String)(implicit c: Connection): Option[Long] =
    SQL("INSERT INTO queuefeatures(name, number) VALUES ({name}, {number})").on('name -> name, 'number -> number).executeInsert()

  def insertFilteredIncall(number: String)(implicit c: Connection) =
    SQL("INSERT INTO filtered_numbers(type, number) VALUES ('incall'::number_type, {number})").on('number -> number).executeUpdate()


  def insertFilteredInternalNumber(number: String)(implicit c: Connection) =
    SQL("INSERT INTO filtered_numbers(type, number) VALUES ('internal_number'::number_type, {number})").on('number -> number).executeUpdate()

  def insertCallElement(callDataId: Long, start: DateTime, end: Option[DateTime], interface: String)(implicit c: Connection) =
    SQL("INSERT INTO call_element(call_data_id, start_time, end_time, interface) VALUES ({id}, {start}, {end}, {interface})")
    .on('id -> callDataId, 'start -> start.toDate, 'end -> end.map(_.toDate), 'interface -> interface).executeUpdate()
}