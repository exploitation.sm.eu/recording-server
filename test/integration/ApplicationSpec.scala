package integration

import helpers.{DbTest, RecordHelper}
import models.DBUtil
import models.authentication.{SupervisorRight, AdminRight}
import org.joda.time.format.DateTimeFormat
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import play.api.cache.Cache
import play.api.db.DB
import play.api.libs.json.{JsArray, Json}
import play.api.test.Helpers._
import play.api.test.{FakeApplication, FakeRequest, WithApplication}

@RunWith(classOf[JUnitRunner])
class ApplicationSpec extends Specification with DbTest{

  val dateFormat = "yyyy-MM-dd HH:mm:ss"
  val formatter = DateTimeFormat.forPattern(dateFormat)

  "Recording server" should {

    "search records and reply in JSON" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      Cache.set("test", AdminRight())
      val req = FakeRequest("POST", "/records/search").withSession(("username", "test"))
                                                      .withJsonBody(Json.obj("callee" -> "2000", "caller" -> "1000", "agent" -> "3000", "queue" -> "thequeue",
                                                                             "start" -> "2013-01-01 15:30:00", "end" -> "2013-01-01 15:40:00", "key" -> "recording" ))
      DB.withConnection(implicit connection => {
        DBUtil.setupDB("record.xml")
        RecordHelper.insertAgent("Pierre", "Dupond", "3000", 0)
        RecordHelper.insertQueue("thequeue", "5000")
        RecordHelper.insertRecord("123", "gw1-123", formatter.parseDateTime("2013-01-01 15:30:21"), Some(formatter.parseDateTime("2013-01-01 15:36:22")), Some("1000"), Some("2000"), Map("key" -> "value"))
        RecordHelper.insertCallOnQueue("123", "3000", "thequeue")
        RecordHelper.insertRecord("456", "gw1-456", formatter.parseDateTime("2013-01-01 15:45:21"), Some(formatter.parseDateTime("2013-01-01 15:58:22")), Some("1001"), Some("2001"))
      })
      val Some(res) = route(req)
      val json = Json.parse(contentAsString(res))
      val records = json \ "records"
      records.as[JsArray].value.size shouldEqual 1
      (records(0) \ "id").as[String] shouldEqual "gw1-123"
      (records(0) \ "start").as[String] shouldEqual "2013-01-01 15:30:21"
      (records(0) \ "duration").as[String] shouldEqual "00:06:01"
      (records(0) \ "src_num").as[String] shouldEqual "1000"
      (records(0) \ "dst_num").as[String] shouldEqual "2000"
      (records(0) \ "agent").as[String] shouldEqual "Pierre Dupond (3000)"
      (records(0) \ "queue").as[String] shouldEqual "thequeue (5000)"
      (records(0) \ "attached_data").as[Map[String, String]] shouldEqual Map("recording" -> "gw1-123",
                                                                              "key" -> "value")
      (json \ "hasNext").asOpt[Boolean] shouldEqual Some(false)
    }

    "search records by callid and reply in JSON" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      Cache.set("test", AdminRight())
      val req = FakeRequest("POST", "/records/callid_search?callid=123").withSession(("username", "test"))
      DB.withConnection(implicit connection => {
        DBUtil.setupDB("record.xml")
        RecordHelper.insertAgent("Pierre", "Dupond", "3000", 0)
        RecordHelper.insertQueue("thequeue", "5000")
        RecordHelper.insertRecord("123", "gw1-123", formatter.parseDateTime("2013-01-01 15:30:21"), Some(formatter.parseDateTime("2013-01-01 15:36:22")), Some("1000"), Some("2000"), Map("key" -> "value"))
        RecordHelper.insertCallOnQueue("123", "3000", "thequeue")
        RecordHelper.insertRecord("456", "gw1-456", formatter.parseDateTime("2013-01-01 15:45:21"), Some(formatter.parseDateTime("2013-01-01 15:58:22")), Some("1001"), Some("2001"))
      })
      val Some(res) = route(req)
      val json = Json.parse(contentAsString(res))
      val records = json \ "records"
      records.as[JsArray].value.size shouldEqual 1
      (records(0) \ "id").as[String] shouldEqual "gw1-123"
      (records(0) \ "start").as[String] shouldEqual "2013-01-01 15:30:21"
      (records(0) \ "duration").as[String] shouldEqual "00:06:01"
      (records(0) \ "src_num").as[String] shouldEqual "1000"
      (records(0) \ "dst_num").as[String] shouldEqual "2000"
      (records(0) \ "agent").as[String] shouldEqual "Pierre Dupond (3000)"
      (records(0) \ "queue").as[String] shouldEqual "thequeue (5000)"
      (records(0) \ "attached_data").as[Map[String, String]] shouldEqual Map("recording" -> "gw1-123",
        "key" -> "value")
        (json \ "hasNext").asOpt[Boolean] shouldEqual Some(false)
    }

    "filter records according to the user's rights" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      val req = FakeRequest("POST", "/records/search").withSession(("username", "test"))
        .withJsonBody(Json.obj("start" -> "2013-01-01 15:30:00", "key" -> "recording" ))
      DB.withConnection(implicit connection => {
        DBUtil.setupDB("record.xml")
        RecordHelper.insertAgent("Pierre", "Dupond", "3000", 0)
        val queueId1 = RecordHelper.insertQueue("thequeue", "5000").get
        Cache.set("test", SupervisorRight(List(queueId1.toInt), List(), List()))
        RecordHelper.insertRecord("123", "gw1-123", formatter.parseDateTime("2013-01-01 15:30:21"), Some(formatter.parseDateTime("2013-01-01 15:36:22")), Some("1000"), Some("2000"), Map("key" -> "value"))
        RecordHelper.insertCallOnQueue("123", "3000", "thequeue")
        RecordHelper.insertRecord("456", "gw1-456", formatter.parseDateTime("2013-01-01 15:45:21"), Some(formatter.parseDateTime("2013-01-01 15:58:22")), Some("1001"), Some("2001"))
        RecordHelper.insertCallOnQueue("456", "3000", "otherqueue")
      })
      val Some(res) = route(req)
      val json = Json.parse(contentAsString(res))
      val records = json \ "records"
      records.as[JsArray].value.size shouldEqual 1
      (records(0) \ "id").as[String] shouldEqual "gw1-123"
      (records(0) \ "start").as[String] shouldEqual "2013-01-01 15:30:21"
      (records(0) \ "duration").as[String] shouldEqual "00:06:01"
      (records(0) \ "src_num").as[String] shouldEqual "1000"
      (records(0) \ "dst_num").as[String] shouldEqual "2000"
      (records(0) \ "agent").as[String] shouldEqual "Pierre Dupond (3000)"
      (records(0) \ "queue").as[String] shouldEqual "thequeue (5000)"
      (records(0) \ "attached_data").as[Map[String, String]] shouldEqual Map("recording" -> "gw1-123",
        "key" -> "value")
      (json \ "hasNext").asOpt[Boolean] shouldEqual Some(false)
    }
  }
}