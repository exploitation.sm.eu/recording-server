package integration

import helpers.{DbTest, RecordHelper}
import models.DBUtil
import models.authentication.AdminRight
import org.joda.time.format.DateTimeFormat
import org.specs2.mutable.Specification
import play.api.cache.Cache
import play.api.db.DB
import play.api.libs.json.{JsArray, Json}
import play.api.test.Helpers._
import play.api.test.{FakeApplication, FakeRequest, WithApplication}

class HistorySpec extends Specification with DbTest {

  val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")

  "History controller" should {
    "search call elements by interface in the database" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      Cache.set("test", AdminRight())
      val req = FakeRequest("POST", "/history")
        .withSession(("username", "test"))
        .withJsonBody(Json.obj("interface" -> "SIP/fndsjl"))
      DB.withConnection(implicit connection => {
        DBUtil.setupDB("record.xml")
        val id1 = RecordHelper.insertCallData("123", formatter.parseDateTime("2013-01-01 15:30:21").toDate,
          Some(formatter.parseDateTime("2013-01-01 15:36:22").toDate), Some("1000"), Some("2000"))
        RecordHelper.insertCallElement(id1.get, formatter.parseDateTime("2013-01-01 15:30:21"), Some(formatter.parseDateTime("2013-01-01 15:36:22")), "SIP/fndsjl")
        val id2 = RecordHelper.insertCallData("456", formatter.parseDateTime("2013-01-01 16:30:21").toDate,
          Some(formatter.parseDateTime("2013-01-01 16:36:22").toDate),Some( "1001"), Some("2001"))
        RecordHelper.insertCallElement(id2.get, formatter.parseDateTime("2013-01-01 16:30:21"), Some(formatter.parseDateTime("2013-01-01 16:36:22")), "SIP/sapanj")
      })

      val Some(res) = route(req)

      val json = Json.parse(contentAsString(res))
      json shouldEqual JsArray(List(Json.obj(
        "start" -> "2013-01-01 15:30:21",
        "duration" -> "00:06:01",
        "src_num" -> "1000",
        "dst_num" -> "2000",
        "status" -> "missed"
      )))
    }
  }
}
