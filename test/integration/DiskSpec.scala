package integration

import helpers.DbTest
import models.authentication.AdminRight
import play.api.test.{FakeApplication, FakeRequest, PlaySpecification, WithApplication}
import play.api.cache.Cache

class DiskSpec extends PlaySpecification with DbTest {

    "The disk application" should {
    "return the list of partitions in JSON" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      Cache.set("test", AdminRight())
      val res = route(FakeRequest("GET", "/partitions").withSession("username" -> "test")).get

      (contentAsJson(res) \\ "name")(0).as[String] must startWith("/dev")
      (contentAsJson(res) \\ "mountPoint")(0).as[String] must startWith("/")
    }
  }
}
