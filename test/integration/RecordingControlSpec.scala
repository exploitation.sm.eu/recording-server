package integration

import helpers.{DbTest, RecordHelper}
import models.authentication.AdminRight
import models.{DBUtil, FilteredIncalls, FilteredInternalNumbers}
import org.joda.time.format.DateTimeFormat
import play.api.cache.Cache
import play.api.libs.json.Json
import play.api.test.{FakeApplication, FakeRequest, PlaySpecification, WithApplication}

class RecordingControlSpec extends PlaySpecification with DbTest {

  val dateFormat = "yyyy-MM-dd HH:mm:ss"
  val formatter = DateTimeFormat.forPattern(dateFormat)
  implicit val connection = DBUtil.getConnection()
  DBUtil.setupDB("record.xml")

  "The RecordingControl Controller" should {
    "list filtered incalls" in new WithApplication(FakeApplication(additionalConfiguration=testConfig)) {
      DBUtil.cleanTable("filtered_numbers")
      RecordHelper.insertFilteredIncall("123456")
      Cache.set("toto", AdminRight())

      val res = route(FakeRequest("GET", "/filtered_incalls").withSession("username" -> "toto")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson(List("123456"))
    }

    "return a single filtered incall" in new WithApplication(FakeApplication(additionalConfiguration=testConfig)) {
      DBUtil.cleanTable("filtered_numbers")
      RecordHelper.insertFilteredIncall("123456")

      val res = route(FakeRequest("GET", "/filtered_incalls/123456")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson("123456")
    }

    "return 404 if the provided number does not exist" in new WithApplication(FakeApplication(additionalConfiguration=testConfig)) {
      DBUtil.cleanTable("filtered_numbers")
      RecordHelper.insertFilteredIncall("123456")

      val res = route(FakeRequest("GET", "/filtered_incalls/456789")).get

      status(res) shouldEqual NOT_FOUND
    }

    "delete a filtered incall" in new WithApplication(FakeApplication(additionalConfiguration=testConfig)) {
      DBUtil.cleanTable("filtered_numbers")
      List("123456", "456789").foreach(RecordHelper.insertFilteredIncall)
      Cache.set("toto", AdminRight())

      val res = route(FakeRequest("DELETE", "/filtered_incalls/123456").withSession("username" -> "toto")).get

      status(res) shouldEqual NO_CONTENT
      FilteredIncalls.all() shouldEqual List("456789")
    }

    "create a filtered incall" in new WithApplication(FakeApplication(additionalConfiguration=testConfig)) {
      DBUtil.cleanTable("filtered_numbers")
      Cache.set("toto", AdminRight())

      val res = route(FakeRequest("POST", "/filtered_incalls").withJsonBody(Json.toJson("123456")).
        withSession("username" -> "toto")).get

      status(res) shouldEqual CREATED
      FilteredIncalls.all() shouldEqual List("123456")
    }

    "return an error when creating an existing filtered incall" in new WithApplication(FakeApplication(additionalConfiguration=testConfig)) {
      DBUtil.cleanTable("filtered_numbers")
      RecordHelper.insertFilteredIncall("123456")
      Cache.set("toto", AdminRight())

      val res = route(FakeRequest("POST", "/filtered_incalls").withJsonBody(Json.toJson("123456")).
        withSession("username" -> "toto")).get

      status(res) shouldEqual BAD_REQUEST
      contentAsJson(res) shouldEqual Json.toJson("Le numéro 123456 existe déjà")
    }

    "list filtered internal numbers" in new WithApplication(FakeApplication(additionalConfiguration=testConfig)) {
      DBUtil.cleanTable("filtered_numbers")
      RecordHelper.insertFilteredInternalNumber("123456")
      Cache.set("toto", AdminRight())

      val res = route(FakeRequest("GET", "/filtered_internal_numbers").withSession("username" -> "toto")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson(List("123456"))
    }

    "return a single filtered internal number" in new WithApplication(FakeApplication(additionalConfiguration=testConfig)) {
      DBUtil.cleanTable("filtered_numbers")
      RecordHelper.insertFilteredInternalNumber("123456")

      val res = route(FakeRequest("GET", "/filtered_internal_numbers/123456")).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson("123456")
    }

    "return 404 if the provided internal number does not exist" in new WithApplication(FakeApplication(additionalConfiguration=testConfig)) {
      DBUtil.cleanTable("filtered_numbers")
      RecordHelper.insertFilteredInternalNumber("123456")

      val res = route(FakeRequest("GET", "/filtered_internal_numbers/456789")).get

      status(res) shouldEqual NOT_FOUND
    }

    "delete a filtered internal number" in new WithApplication(FakeApplication(additionalConfiguration=testConfig)) {
      DBUtil.cleanTable("filtered_numbers")
      List("123456", "456789").foreach(RecordHelper.insertFilteredInternalNumber)
      Cache.set("toto", AdminRight())

      val res = route(FakeRequest("DELETE", "/filtered_internal_numbers/123456").withSession("username" -> "toto")).get

      status(res) shouldEqual NO_CONTENT
      FilteredInternalNumbers.all() shouldEqual List("456789")
    }

    "create a filtered internal number" in new WithApplication(FakeApplication(additionalConfiguration=testConfig)) {
      DBUtil.cleanTable("filtered_numbers")
      Cache.set("toto", AdminRight())

      val res = route(FakeRequest("POST", "/filtered_internal_numbers").withJsonBody(Json.toJson("123456")).
        withSession("username" -> "toto")).get

      status(res) shouldEqual CREATED
      FilteredInternalNumbers.all() shouldEqual List("123456")
    }

    "return an error when creating an existing filtered internal number" in new WithApplication(FakeApplication(additionalConfiguration=testConfig)) {
      DBUtil.cleanTable("filtered_numbers")
      RecordHelper.insertFilteredInternalNumber("123456")
      Cache.set("toto", AdminRight())

      val res = route(FakeRequest("POST", "/filtered_internal_numbers").withJsonBody(Json.toJson("123456")).
        withSession("username" -> "toto")).get

      status(res) shouldEqual BAD_REQUEST
      contentAsJson(res) shouldEqual Json.toJson("Le numéro 123456 existe déjà")
    }
  }
}
