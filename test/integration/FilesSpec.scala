package integration

import java.io.File
import helpers.DbTest
import models.authentication.AdminRight
import org.joda.time.format.DateTimeFormat
import play.api.cache.Cache
import play.api.test.{WithApplication, FakeApplication, FakeRequest, PlaySpecification}

class FilesSpec extends PlaySpecification with DbTest {

  val dateFormat = "yyyy-MM-dd HH:mm:ss"
  val formatter = DateTimeFormat.forPattern(dateFormat)
  override def testConfig = super.testConfig ++ Map(
    "audio.folder" -> "/tmp",
    "audio.extensions" -> List("wav", "truc"))

  "The Files controller" should {

    "send a file if the file exists" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      new File("/tmp/gw1").mkdir()
      new File("/tmp/gw1/gw1-123456.truc").createNewFile()

      val req = FakeRequest("GET", "/records/gw1-123456/audio").withSession(("username", "test"))
      val Some(res) = route(req)

      status(res) shouldEqual 200
    }

    "return 404 if the file does not exist" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      Cache.set("test", AdminRight())
      new File("/tmp/gw1").mkdir()
      val fichier = new File("/tmp/gw1/gw1-123456.truc")
      if (fichier.exists())
        fichier.delete()

      val req = FakeRequest("GET", "/records/gw1-123456/audio").withSession(("username", "test"))
      val Some(res) = route(req)

      status(res) shouldEqual 404
    }

    "redirect to the login page if no session is found" in new WithApplication(FakeApplication(additionalConfiguration = testConfig)) {
      val req = FakeRequest("GET", "/records/gw1-123456/audio")
      val Some(res) = route(req)
      redirectLocation(res).get must contain("login")
    }
  }
}