package xivo.recording

import helpers.{DbTest, RecordHelper}
import models.{CallDirection, DBUtil}
import org.joda.time.DateTime
import play.api.test.{FakeApplication, PlaySpecification}
import java.io.File
import play.Application

class SoundFileRemoverSpec extends PlaySpecification with DbTest {

  val xivoAudioFolder = testConfig("audio.folder") + "/xivo"
  implicit val connection = DBUtil.getConnection()

  "A SoundFileRemover" should {
    "delete orphan sound files" in {
      cleanDirectory(xivoAudioFolder)
      val tooRecentFile = "xivo-12344.wav"
      val fileNotToDelete = "xivo-12355.wav"
      val fileToDelete = "xivo-12366.wav"
      val tooOldFile = "xivo-12377.wav"

      DBUtil.setupDB("record.xml")
      RecordHelper.insertRecord("12355", "xivo-12355", new DateTime().minusDays(1).minusHours(2), Some(new DateTime().minusDays(1).minusHours(1)), Some("1000"), Some("2000"))
      RecordHelper.insertCallData("789456", new DateTime().minusHours(3).toDate, Some(new DateTime().minusHours(3).toDate), Some("1000"), Some("2000"), CallDirection.Incoming)

      createFileWithDate(tooRecentFile, new DateTime().minusHours(1))
      createFileWithDate(fileNotToDelete, new DateTime().minusDays(1).minusHours(2))
      createFileWithDate(fileToDelete, new DateTime().minusDays(1).minusHours(2))
      createFileWithDate(tooOldFile, new DateTime().minusDays(2).minusHours(2))

      new SoundFileRemover(new Application(FakeApplication(additionalConfiguration = testConfig))).run()

      new File(xivoAudioFolder).listFiles().map(f => f.getName).sorted shouldEqual Array(tooRecentFile, fileNotToDelete, tooOldFile).sorted
    }
  }

  "not delete orphan files if there is no recent data in the database" in {
    cleanDirectory(xivoAudioFolder)
    val fileToDelete = "xivo-12366.wav"
    DBUtil.setupDB("record.xml")
    createFileWithDate(fileToDelete, new DateTime().minusDays(1).minusHours(2))

    new SoundFileRemover(new Application(FakeApplication(additionalConfiguration = testConfig))).run()

    new File(xivoAudioFolder).listFiles().map(f => f.getName) shouldEqual Array(fileToDelete)
  }

  def createFileWithDate(fileName: String, date: DateTime): Unit = {
    val file = new File(s"$xivoAudioFolder/$fileName")
    file.createNewFile()
    file.setLastModified(date.toDate.getTime)
  }

  def cleanDirectory(dirName: String): Unit = {
    val dir = new File(dirName)
    if(!dir.isDirectory) {
      dir.delete()
      dir.mkdirs()
    }
    for(file <- dir.listFiles())
      file.delete()
  }
}
