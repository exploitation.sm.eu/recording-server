package models

import helpers.RecordHelper
import play.api.test.PlaySpecification

class FilteredIncallsSpec extends PlaySpecification {

  implicit val connection = DBUtil.getConnection()
  DBUtil.setupDB("record.xml")

  "The FilteredIncalls Singleton" should {
    "list all filtered incalls" in {
      DBUtil.cleanTable("filtered_numbers")
      List("123456", "456789").foreach(RecordHelper.insertFilteredIncall)

      FilteredIncalls.all().sorted shouldEqual List("123456", "456789")
    }

    "find a filtered incall by number" in {
      DBUtil.cleanTable("filtered_numbers")
      List("123456", "456789").foreach(RecordHelper.insertFilteredIncall)

      FilteredIncalls.find("123456") shouldEqual Some("123456")
    }

    "return None if the provided number does not exist" in {
      DBUtil.cleanTable("filtered_numbers")
      List("456789").foreach(RecordHelper.insertFilteredIncall)

      FilteredIncalls.find("123456") shouldEqual None
    }

    "delete a filtered incall" in {
      DBUtil.cleanTable("filtered_numbers")
      List("123456", "456789").foreach(RecordHelper.insertFilteredIncall)

      FilteredIncalls.delete("123456")

      FilteredIncalls.all() shouldEqual List("456789")
    }

    "create a filtered incall" in {
      DBUtil.cleanTable("filtered_numbers")

      FilteredIncalls.create("123456")

      FilteredIncalls.all() shouldEqual List("123456")
    }

    "throw an exception if the filtered incall already exists" in {
      DBUtil.cleanTable("filtered_numbers")
      FilteredIncalls.create("123456")
      FilteredIncalls.create("123456") should throwA[NumberAlreadyExistsException]
    }
  }
}