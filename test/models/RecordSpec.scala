package models

import helpers.RecordHelper
import models.authentication.{AdminRight, LimitedRight}
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.specs2.mutable.Before
import play.api.test.PlaySpecification

class RecordSpec extends PlaySpecification {

  val dateFormat = "yyyy-MM-dd HH:mm:ss"
  val formatter = DateTimeFormat.forPattern(dateFormat)
  implicit val connection = DBUtil.connection
  implicit val paginator = Paginator(0, 20)
  DBUtil.setupDB("record.xml")
  List("agentfeatures", "agentgroup", "extensions", "queuefeatures").foreach(DBUtil.cleanTable)
  val groupId1 = RecordHelper.insertAgentGroup("group1").get.toInt
  val groupId2 = RecordHelper.insertAgentGroup("group2").get.toInt
  val agentId1 = RecordHelper.insertAgent("Pierre", "Martin", "3000", groupId1).get.toInt
  val agentId2 = RecordHelper.insertAgent("Jean", "Dupond", "3001", groupId1).get.toInt
  val agentId3 = RecordHelper.insertAgent("Jean", "Martin", "3002", groupId2).get.toInt
  val agentId4 = RecordHelper.insertAgent("Philippe", "Joli", "3003", groupId2).get.toInt
  val queueId1 = RecordHelper.insertQueue("thequeue", "5000").get.toInt
  val queueId2 = RecordHelper.insertQueue("une_file", "1234").get.toInt
  val incallId1 = RecordHelper.insertIncall("5300").get.toInt
  val incallId2 = RecordHelper.insertIncall("5301").get.toInt

  trait clean extends Before {
    val tables = List("attached_data", "call_data", "call_on_queue", "call_element")
    def before = {
      for (table <- tables)
        DBUtil.cleanTable(table)
    }
  }

  "The Record singleton" should {

    "search records with three criteria" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "456.789"
      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1000"), Some("2000"))
      RecordHelper.insertCallOnQueue(id1, "3000", "thequeue")
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("1001"), Some("2001"))
      RecordHelper.insertCallOnQueue(id2, "3001", "thequeue")

      val records = Record.search(GenericSearchCriteria(Some("2000"), Some("1000"), Some("3000"), None, None, None, CallDirection.All, Some("recording")), AdminRight())

      records.size shouldEqual 1
      records(0).recordId shouldEqual Some(s"gw1-$id1")
      records(0).agent shouldEqual "Pierre Martin (3000)"
      records(0).queue shouldEqual "thequeue (5000)"
    }

    "search records with two criteria" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "456.789"
      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1000"), None)
      RecordHelper.insertCallOnQueue(id1, "3000", "thequeue")
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("1001"), Some("2001"))
      RecordHelper.insertCallOnQueue(id2, "3001", "thequeue")

      val records = Record.search(GenericSearchCriteria(None, Some("1000"), Some("3000"), None, None, None, CallDirection.All, Some("recording")), AdminRight())

      records.size shouldEqual 1
      records(0).recordId shouldEqual Some(s"gw1-$id1")
    }

    "search records with one criterion" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "456.789"
      val id3 = "789.123"
      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, None, Some("1000"), Some("2000"))
      RecordHelper.insertCallOnQueue(id1, "3000", "thequeue")
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("1001"), Some("2001"))
      RecordHelper.insertCallOnQueue(id2, "3001", "thequeue")
      RecordHelper.insertRecord(id3, s"gw1-$id3", startDate, Some(endDate), None, Some("2000"))
      RecordHelper.insertCallOnQueue(id3, "3000", "thequeue")

      val records = Record.search(GenericSearchCriteria(None, None, Some("3000"), None, None, None, CallDirection.All, Some("recording")), AdminRight())

      records.size shouldEqual 1
      records(0).recordId shouldEqual Some(s"gw1-$id3")
    }

    "find records by agent even if they are not on the first page" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "456.789"
      implicit val paginator = Paginator(0,1)
      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1000"), Some("2000"))
      RecordHelper.insertCallOnQueue(id1, "3001", "thequeue")
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("1001"), Some("2001"))
      RecordHelper.insertCallOnQueue(id2, "3000", "thequeue")

      val records = Record.search(GenericSearchCriteria(None, None, Some("3000"), None, None, None, CallDirection.All, Some("recording")), AdminRight())

      records.size shouldEqual 1
      records(0).recordId shouldEqual Some(s"gw1-$id2")
    }

    "search records by queue" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "456.789"

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("2001"))
      RecordHelper.insertCallOnQueue(id1, "3001", "myqueue")
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("1000"), Some("2000"))
      RecordHelper.insertCallOnQueue(id2, "3000", "thequeue")

      val records = Record.search(GenericSearchCriteria(None, None, None, Some("thequeue"), None, None, CallDirection.All, Some("recording")), AdminRight())

      records.size shouldEqual 1
      records(0).recordId shouldEqual Some(s"gw1-$id2")
    }

    "search and return paginated data" in new clean {
      var startDate = DateTime.now
      var endDate = startDate.plusMinutes(5)
      for (i <- 0 to 19) {
        RecordHelper.insertRecord(i.toString, s"gw1-$i", startDate.plusSeconds(19-i), Some(endDate), Some("1000"), Some("2000"))
      }
      RecordHelper.insertCallData("22", startDate.toDate, Some(endDate.toDate), Some("1000"), Some("2000"))

      implicit var paginator = Paginator(0, 10)
      val records = Record.search(GenericSearchCriteria(None, Some("1000"), None, None, None, None, CallDirection.All, Some("recording")), AdminRight())
      records.size shouldEqual 10
      records(0).recordId shouldEqual Some("gw1-0")
      records(9).recordId shouldEqual Some("gw1-9")

      paginator = Paginator(10, 10)
      val records2 = Record.search(GenericSearchCriteria(None, Some("1000"), None, None, None, None, CallDirection.All, Some("recording")), AdminRight())
      records2.size shouldEqual 10
      records2(0).recordId shouldEqual Some("gw1-10")
      records2(9).recordId shouldEqual Some("gw1-19")
    }

    "search records between two dates" in new clean {
      val startSearch = formatter.parseDateTime("2013-05-12 14:00:00")
      val endSearch = formatter.parseDateTime("2013-05-12 15:00:00")

      val avantStart1 = formatter.parseDateTime("2013-05-12 13:30:00")
      val avantStart2 = formatter.parseDateTime("2013-05-12 13:45:00")
      val entreStartEnd1 = formatter.parseDateTime("2013-05-12 14:15:00")
      val entreStartEnd2 = formatter.parseDateTime("2013-05-12 14:30:00")
      val apresEnd1 = formatter.parseDateTime("2013-05-12 15:15:00")
      val apresEnd2 = formatter.parseDateTime("2013-05-12 15:30:00")

      RecordHelper.insertRecord("11", "gw1-11", avantStart1, Some(avantStart2), Some("1000"), Some("2000"))
      RecordHelper.insertRecord("22", "gw1-22", avantStart1, Some(entreStartEnd1), Some("1000"), Some("2000"))
      RecordHelper.insertRecord("33", "gw1-33", entreStartEnd1, Some(entreStartEnd2), Some("1000"), Some("2000"))
      RecordHelper.insertRecord("44", "gw1-44", entreStartEnd2, Some(apresEnd1), Some("1000"), Some("2000"))
      RecordHelper.insertRecord("55", "gw1-55", apresEnd1, Some(apresEnd2), Some("1000"), Some("2000"))
      RecordHelper.insertRecord("66", "gw1-66", avantStart1, Some(apresEnd1), Some("1000"), Some("2000"))

      implicit var paginator = Paginator(0, 10)
      val records = Record.search(GenericSearchCriteria(None, None, None, None, Some(startSearch), Some(endSearch), CallDirection.All, Some("recording")), AdminRight())
      records.size shouldEqual 4
      val ids = records.flatMap(r => r.recordId)
      ids should contain("gw1-22")
      ids should contain("gw1-33")
      ids should contain("gw1-44")
      ids should contain("gw1-66")
    }

    "retrieve records with all its attached data" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id = "123.456"
      val data = Map("key1" -> "value1",
                     "key2" -> "value2")

      RecordHelper.insertRecord(id, s"gw1-$id", startDate, Some(endDate), Some("1001"), Some("2001"), data)

      val records = Record.search(GenericSearchCriteria(None, None, None, None, None, None, CallDirection.All, Some("recording")), AdminRight())

      records.size shouldEqual 1
      records(0).recordId shouldEqual Some(s"gw1-$id")
      records(0).attachedData shouldEqual Map("key1" -> "value1",
                                              "key2" -> "value2",
                                              "recording" -> s"gw1-$id")
    }

    "search only incoming calls" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "123.457"

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("2001"), direction=CallDirection.Incoming)
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("1001"), Some("2001"), direction=CallDirection.Outgoing)

      val records = Record.search(GenericSearchCriteria(None, None, None, None, None, None, CallDirection.Incoming, Some("recording")), AdminRight())

      records.size shouldEqual 1
      records(0).recordId shouldEqual Some(s"gw1-$id1")
    }

    "search only outgoing calls" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "123.457"

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("2001"), direction=CallDirection.Incoming)
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("1001"), Some("2001"), direction=CallDirection.Outgoing)

      val records = Record.search(GenericSearchCriteria(None, None, None, None, None, None, CallDirection.Outgoing, Some("recording")), AdminRight())

      records.size shouldEqual 1
      records(0).recordId shouldEqual Some(s"gw1-$id2")
    }

    "search by substring in agent name, firstname and number" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "123.457"
      val id3 = "123.458"
      val id4 = "123.459"

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("2001"), direction=CallDirection.Incoming)
      RecordHelper.insertCallOnQueue(id1, "3000", "the_queue")
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("1001"), Some("2001"), direction=CallDirection.Outgoing)
      RecordHelper.insertCallOnQueue(id2, "3001", "the_queue")
      RecordHelper.insertRecord(id3, s"gw1-$id3", startDate, Some(endDate), Some("1001"), Some("2001"), direction=CallDirection.Incoming, dstAgent=Some("3002"))
      RecordHelper.insertRecord(id4, s"gw1-$id4", startDate, Some(endDate), Some("1001"), Some("2001"), direction=CallDirection.Outgoing, srcAgent=Some("3003"))

      Record.search(GenericSearchCriteria(None, None, Some("300"), None, None, None, CallDirection.All, Some("recording")), AdminRight())
        .flatMap(r => r.recordId).sorted shouldEqual List(s"gw1-$id1", s"gw1-$id2", s"gw1-$id3", s"gw1-$id4").sorted

      Record.search(GenericSearchCriteria(None, None, Some("ierr"), None, None, None, CallDirection.All, Some("recording")), AdminRight())
        .flatMap(r => r.recordId).sorted shouldEqual List(s"gw1-$id1")

      Record.search(GenericSearchCriteria(None, None, Some("dupo"), None, None, None, CallDirection.All, Some("recording")), AdminRight())
        .flatMap(r => r.recordId).sorted shouldEqual List(s"gw1-$id2")

      Record.search(GenericSearchCriteria(None, None, Some("jol"), None, None, None, CallDirection.All, Some("recording")), AdminRight())
        .flatMap(r => r.recordId).sorted shouldEqual List(s"gw1-$id4")

      Record.search(GenericSearchCriteria(None, None, Some("3002"), None, None, None, CallDirection.All, Some("recording")), AdminRight())
        .flatMap(r => r.recordId).sorted shouldEqual List(s"gw1-$id3")
    }

    "search by callid" in new clean {
      RecordHelper.insertRecord("123", "gw1-123", DateTime.now, Some(DateTime.now.plusMinutes(5)), Some("1001"), Some("2001"), direction=CallDirection.Incoming)
      RecordHelper.insertCallOnQueue("123", "3000", "the_queue")

      Record.searchByCallid("123", AdminRight()).flatMap(r => r.recordId) shouldEqual List("gw1-123")
    }

    "tell if a globalId exists" in new clean {
      RecordHelper.insertRecord("123", "gw1-123", DateTime.now, Some(DateTime.now.plusMinutes(5)), Some("1001"), Some("2001"), direction=CallDirection.Incoming)

      Record.globalIdExists("gw1-123") should beTrue
      Record.globalIdExists("gw1-456") should beFalse
    }

    "return the call uniqueidfor the given globalId" in new clean {
      RecordHelper.insertRecord("123", "gw1-123", DateTime.now, Some(DateTime.now.plusMinutes(5)), Some("1001"), Some("2001"), direction=CallDirection.Incoming)

      Record.callidByGlobalId("gw1-123") shouldEqual Some("123")
      Record.callidByGlobalId("gw1-456") shouldEqual None
    }

    "search by substring in queue name and number" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "123.457"

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("2001"), direction=CallDirection.Incoming)
      RecordHelper.insertCallOnQueue(id1, "3000", "thequeue")
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("1001"), Some("2001"), direction=CallDirection.Outgoing)
      RecordHelper.insertCallOnQueue(id2, "3001", "une_file")

      Record.search(GenericSearchCriteria(None, None, Some("300"), None, None, None, CallDirection.All, Some("recording")), AdminRight())
        .flatMap(r => r.recordId).sorted shouldEqual List(s"gw1-$id1", s"gw1-$id2").sorted

      Record.search(GenericSearchCriteria(None, None, None, Some("23"), None, None, CallDirection.All, Some("recording")), AdminRight())
        .flatMap(r => r.recordId).sorted shouldEqual List(s"gw1-$id2")

      Record.search(GenericSearchCriteria(None, None, None, Some("hequ"), None, None, CallDirection.All, Some("recording")), AdminRight())
        .flatMap(r => r.recordId).sorted shouldEqual List(s"gw1-$id1")
    }

    "use the agent number provided by call_data if there is no call_on_queue" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "123.457"
      val id3 = "123.458"

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("2001"), direction=CallDirection.Incoming)
      RecordHelper.insertCallOnQueue(id1, "3000", "thequeue")
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("3000"), Some("1001"), direction=CallDirection.Outgoing, srcAgent = Some("3000"))
      RecordHelper.insertRecord(id3, s"gw1-$id3", startDate, Some(endDate), Some("1001"), Some("3000"), direction=CallDirection.Incoming, dstAgent = Some("3000"))

      Record.search(GenericSearchCriteria(None, None, None, None, Some(DateTime.now()), None, CallDirection.All, Some("recording")), AdminRight()).map(r => r.agent) shouldEqual
        List.fill(3)("Pierre Martin (3000)")
    }

    "search the calls with an attached data containing the given key" in new clean {
      val callDataId = RecordHelper.insertCallData("123", DateTime.now.toDate, Some(DateTime.now.plusMinutes(5).toDate), Some("1001"), Some("2001"), direction=CallDirection.Incoming)
      RecordHelper.insertAttachedData(callDataId.get, "the_key", "the_value")
      RecordHelper.insertCallData("456", DateTime.now.toDate, Some(DateTime.now.plusMinutes(5).toDate), Some("2001"), Some("3001"), direction=CallDirection.Incoming)

      val sc = GenericSearchCriteria(None, None, None, None, Some(DateTime.now()), None, CallDirection.All, Some("the_key"))
      Record.search(sc, AdminRight()).map(_.callDataId) shouldEqual  List(callDataId.get)
    }

    "return only one row if there are several attached data" in new clean {
      val callDataId = RecordHelper.insertCallData("123", DateTime.now.toDate, Some(DateTime.now.plusMinutes(5).toDate), Some("1001"), Some("2001"), direction=CallDirection.Incoming)
      RecordHelper.insertAttachedData(callDataId.get, "the_key", "the_value")
      RecordHelper.insertAttachedData(callDataId.get, "other_key", "other_value")

      val sc = GenericSearchCriteria(None, None, None, None, Some(DateTime.now()), None, CallDirection.All, None)
      Record.search(sc, AdminRight()).map(_.callDataId) shouldEqual  List(callDataId.get)
    }

    "filter the rows by agent group when the user has limited rights" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "123.457"
      val id3 = "123.458"
      val right = new LimitedRight(List(), List(groupId1), List()) {}

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("2001"), direction=CallDirection.Incoming)
      RecordHelper.insertCallOnQueue(id1, "3000", "thequeue")
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("3000"), Some("1001"), direction=CallDirection.Outgoing, srcAgent = Some("3001"))
      RecordHelper.insertRecord(id3, s"gw1-$id3", startDate, Some(endDate), Some("1001"), Some("3000"), direction=CallDirection.Incoming, dstAgent = Some("3002"))

      Record.search(GenericSearchCriteria(None, None, None, None, Some(DateTime.now()), None, CallDirection.All, None), right).flatMap(r => r.recordId).sorted shouldEqual
        List(s"gw1-$id1", s"gw1-$id2").sorted
    }

    "filter the rows by queue when the user is Supervisor" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "123.457"
      val id3 = "123.458"
      val right = new LimitedRight(List(queueId1), List(), List()){}

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("2001"), direction=CallDirection.Incoming)
      RecordHelper.insertCallOnQueue(id1, "3000", "thequeue")
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("3000"), Some("1001"), direction=CallDirection.Outgoing, srcAgent = Some("3001"))
      RecordHelper.insertCallOnQueue(id2, "3000", "une_file")
      RecordHelper.insertRecord(id3, s"gw1-$id3", startDate, Some(endDate), Some("1001"), Some("3000"), direction=CallDirection.Incoming, dstAgent = Some("3002"))

      Record.search(GenericSearchCriteria(None, None, None, None, Some(DateTime.now()), None, CallDirection.All, None), right).flatMap(r => r.recordId) shouldEqual
        List(s"gw1-$id1")
    }

    "filter the rows by incall when the user is Supervisor" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "123.457"
      val id3 = "123.458"
      val right = new LimitedRight(List(), List(), List(incallId1)){}

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("5300"), direction=CallDirection.Incoming)
      RecordHelper.insertCallOnQueue(id1, "3000", "thequeue")
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("3000"), Some("5300"), direction=CallDirection.Outgoing, srcAgent = Some("3001"))
      RecordHelper.insertCallOnQueue(id2, "3000", "une_file")
      RecordHelper.insertRecord(id3, s"gw1-$id3", startDate, Some(endDate), Some("1001"), Some("5301"), direction=CallDirection.Incoming, dstAgent = Some("3002"))

      Record.search(GenericSearchCriteria(None, None, None, None, Some(DateTime.now()), None, CallDirection.All, None), right).flatMap(r => r.recordId).sorted shouldEqual
        List(s"gw1-$id1", s"gw1-$id2").sorted
    }

    "return nothing if the Supervisor has no rights" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "123.457"
      val id3 = "123.458"
      val right = new LimitedRight(List(), List(), List()){}

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("5300"), direction=CallDirection.Incoming)
      RecordHelper.insertCallOnQueue(id1, "3000", "thequeue")
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("3000"), Some("5300"), direction=CallDirection.Outgoing, srcAgent = Some("3001"))
      RecordHelper.insertCallOnQueue(id2, "3000", "une_file")
      RecordHelper.insertRecord(id3, s"gw1-$id3", startDate, Some(endDate), Some("1001"), Some("5301"), direction=CallDirection.Incoming, dstAgent = Some("3002"))

      Record.search(GenericSearchCriteria(None, None, None, None, Some(DateTime.now()), None, CallDirection.All, None), right) shouldEqual List()
    }

    "filter nothing for an Administrator" in new clean {
      val startDate = DateTime.now
      val endDate = startDate.plusMinutes(5)
      val id1 = "123.456"
      val id2 = "123.457"
      val id3 = "123.458"
      val right = AdminRight()

      RecordHelper.insertRecord(id1, s"gw1-$id1", startDate, Some(endDate), Some("1001"), Some("5300"), direction=CallDirection.Incoming)
      RecordHelper.insertCallOnQueue(id1, "3000", "thequeue")
      RecordHelper.insertRecord(id2, s"gw1-$id2", startDate, Some(endDate), Some("3000"), Some("5300"), direction=CallDirection.Outgoing, srcAgent = Some("3001"))
      RecordHelper.insertCallOnQueue(id2, "3000", "une_file")
      RecordHelper.insertRecord(id3, s"gw1-$id3", startDate, Some(endDate), Some("1001"), Some("5301"), direction=CallDirection.Incoming, dstAgent = Some("3002"))

      Record.search(GenericSearchCriteria(None, None, None, None, Some(DateTime.now()), None, CallDirection.All, None), right).flatMap(r => r.recordId) shouldEqual
        List(s"gw1-$id3", s"gw1-$id2", s"gw1-$id1")
    }
  }
}