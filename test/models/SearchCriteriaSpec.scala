package models

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import play.api.libs.json._
import play.api.test.PlaySpecification

class SearchCriteriaSpec extends PlaySpecification {

  "The SearchCriteria singleton" should {

    val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")

    "parse the Json" in {
      val json = Json.parse("""{
              "callee": "1000",
              "caller": "2000",
              "agent": "3000",
              "queue": "thequeue",
              "start": "2012-05-13 14:15:16",
              "end": "2012-05-13 19:15:16",
              "direction": "incoming",
              "key": "my_key"
      }""")
      SearchCriteria.fromJson(json) shouldEqual GenericSearchCriteria(Some("1000"),
                                                               Some("2000"),
                                                               Some("3000"),
                                                               Some("thequeue"),
                                                               Some(DateTime.parse("2012-05-13 14:15:16", formatter)),
                                                               Some(DateTime.parse("2012-05-13 19:15:16", formatter)),
                                                               CallDirection.Incoming,
                                                               Some("my_key"))
    }

    "not fail with a missing parameter" in {
      val json = Json.parse("""{
              "caller": "2000",
              "agent": "3000"
      }""")
      SearchCriteria.fromJson(json) shouldEqual GenericSearchCriteria(None, Some("2000"), Some("3000"), None, None, None, CallDirection.All, None)
    }

    "not fail with an improperly formatted date" in {
      val json = Json.parse("""{
              "start": "14:15:16 13-05-2013",
              "end": "14:15:16 13-05-2013"
      }""")


      SearchCriteria.fromJson(json) shouldEqual GenericSearchCriteria(None, None, None, None, None, None, CallDirection.All, None)
    }
  }

}
