package models

import helpers.RecordHelper
import play.api.test.PlaySpecification

class XivoObjectsSpec extends PlaySpecification {

  implicit val connection = DBUtil.getConnection()
  DBUtil.setupDB("record.xml")

  "XivoObjects" should {
    "return the queue names for a given pattern" in {
      RecordHelper.insertQueue("car_rental", "3001")
      RecordHelper.insertQueue("homes_selling", "3002")

      XivoObjects.queueNamesForPattern("RENT") shouldEqual List("car_rental")
      XivoObjects.queueNamesForPattern("00").sorted shouldEqual List("car_rental", "homes_selling").sorted
    }

    "return the agent numbers for a given pattern" in {
      RecordHelper.insertAgent("Pierre", "Dupond", "5000", 0)
      RecordHelper.insertAgent("Jean", "Martin", "5001", 0)

      XivoObjects.agentsNumbersForPattern("00").sorted shouldEqual List("5000", "5001").sorted
      XivoObjects.agentsNumbersForPattern("ARTI").sorted shouldEqual List("5001").sorted
      XivoObjects.agentsNumbersForPattern("ierr").sorted shouldEqual List("5000").sorted
    }
  }
}
