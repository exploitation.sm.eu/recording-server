package models

import play.api.test.PlaySpecification

class PartitionSpec extends PlaySpecification {

  "The Partition singleton" should {
    "list partitions" in {
      val res = Partition.all
      res.size must be greaterThanOrEqualTo 1
      res(0).name must be startingWith "/dev"
      res(0).mountPoint must be startingWith "/"
    }
  }
}
