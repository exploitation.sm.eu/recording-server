package models

import helpers.RecordHelper
import org.joda.time.Duration
import org.joda.time.format.DateTimeFormat
import org.specs2.mutable.Before
import play.api.test.PlaySpecification

class CallDetailSpec extends PlaySpecification {
  val dateFormat = "yyyy-MM-dd HH:mm:ss"
  val formatter = DateTimeFormat.forPattern(dateFormat)
  implicit val connection = DBUtil.connection
  implicit val paginator = Paginator(0, 20)
  DBUtil.setupDB("record.xml")
  List("agentfeatures", "agentgroup", "extensions", "queuefeatures").foreach(DBUtil.cleanTable)

  trait clean extends Before {
    val tables = List("attached_data", "call_data", "call_on_queue", "call_element")
    def before = {
      for (table <- tables)
        DBUtil.cleanTable(table)
    }
  }

  "search the call history by peer" in new clean {
    val interface = "SIP/jnfkoz"
    val id1 = RecordHelper.insertCallData("123456.789", formatter.parseDateTime("2014-01-01 08:00:00").toDate,
      Some(formatter.parseDateTime("2014-01-01 08:12:00").toDate), srcNum = Some("1000"), dstNum = None)
    RecordHelper.insertCallElement(id1.get, formatter.parseDateTime("2014-01-01 08:00:01"), Some(formatter.parseDateTime("2014-01-01 08:00:31")), interface)
    RecordHelper.insertCallElement(id1.get, formatter.parseDateTime("2014-01-01 08:00:31"), Some(formatter.parseDateTime("2014-01-01 08:12:00")), "SIP/oabfde")
    val id2 = RecordHelper.insertCallData("123456.795", formatter.parseDateTime("2014-01-01 09:00:00").toDate,
      Some(formatter.parseDateTime("2014-01-01 09:12:00").toDate), srcInterface = Some(interface), srcNum=None, dstNum=Some("2001"))
    val id3 = RecordHelper.insertCallData("123456.784", formatter.parseDateTime("2014-01-01 09:01:00").toDate,
      Some(formatter.parseDateTime("2014-01-01 09:08:00").toDate), srcInterface = Some("SIP/mljdad"))

    CallDetail.historyByInterface(interface, 10) should containAllOf(List(
      CallDetail(formatter.parseDateTime("2014-01-01 08:00:01"), Some(new Duration(formatter.parseDateTime("2014-01-01 08:00:01"),
        formatter.parseDateTime("2014-01-01 08:00:31"))), Some("1000"), None, CallStatus.Missed),
      CallDetail(formatter.parseDateTime("2014-01-01 09:00:00"), Some(new Duration(formatter.parseDateTime("2014-01-01 09:00:00"),
        formatter.parseDateTime("2014-01-01 09:12:00"))), None, Some("2001"), CallStatus.Emitted)
    ))
  }

  "limit the search to the given size" in new clean {
    val interface = "SIP/jnfkoz"
    val id1 = RecordHelper.insertCallData("123456.795", formatter.parseDateTime("2014-01-01 09:00:00").toDate,
      Some(formatter.parseDateTime("2014-01-01 09:12:00").toDate), srcInterface = Some(interface), srcNum=Some("1001"), dstNum=Some("2001"))
    val id2 = RecordHelper.insertCallData("123456.796", formatter.parseDateTime("2014-01-01 09:30:00").toDate,
      Some(formatter.parseDateTime("2014-01-01 09:45:00").toDate), srcInterface = Some(interface), srcNum=Some("1001"), dstNum=Some("2001"))

    CallDetail.historyByInterface(interface, 1) should containAllOf(List(
      CallDetail(formatter.parseDateTime("2014-01-01 09:30:00"), Some(new Duration(formatter.parseDateTime("2014-01-01 09:30:00"),
        formatter.parseDateTime("2014-01-01 09:45:00"))), Some("1001"), Some("2001"), CallStatus.Emitted)))
  }
}
