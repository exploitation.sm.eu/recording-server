package models

import java.sql.{Connection, DriverManager}
import java.util.Properties

import anorm.SQL
import org.dbunit.database.DatabaseConnection
import org.dbunit.dataset.xml.{FlatXmlDataSet, FlatXmlProducer}
import org.dbunit.operation.DatabaseOperation
import org.xml.sax.InputSource

object DBUtil {

  val HOST = "localhost"
  val DB_NAME = "asterisktest"
  val USER = "asterisk"
  val PASSWORD = "asterisk"
  val connection = getConnection

  def getConnection(): Connection = {
    Class.forName("org.postgresql.Driver")
    val uri = "jdbc:postgresql://" + HOST + "/" + DB_NAME
    val props = new Properties()
    props.setProperty("user", USER)
    props.setProperty("password", PASSWORD)
    props.setProperty("stringtype", "unspecified")
    DriverManager.getConnection(uri, props)
  }

  def setupDB(filename: String)(implicit connection: Connection) = {
    var dataset = new FlatXmlDataSet(new FlatXmlProducer(new InputSource(getClass.getClassLoader.getResourceAsStream(filename))))
    for (table <- dataset.getTableNames) {
      val createTable = scala.io.Source.fromInputStream(getClass.getClassLoader.getResourceAsStream(s"$table.sql")).mkString
      SQL(createTable).execute
    }
    val dbunitConnection: DatabaseConnection = new DatabaseConnection(connection)
    DatabaseOperation.CLEAN_INSERT.execute(dbunitConnection, dataset)
  }

  def cleanTable(tableName: String)(implicit connection: Connection) {
    var st = connection.createStatement()
    st.executeUpdate(s"TRUNCATE TABLE $tableName CASCADE")
  }

}