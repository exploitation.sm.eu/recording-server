package models

import helpers.RecordHelper
import org.joda.time.format.DateTimeFormat
import play.api.test.PlaySpecification

class CallsSpec extends PlaySpecification {

  val dateFormat = "yyyy-MM-dd HH:mm:ss"
  val formatter = DateTimeFormat.forPattern(dateFormat)
  implicit val connection = DBUtil.getConnection()

  "The Calls singleton" should {
    "tell if there is a call more recent than the given date" in {
      DBUtil.setupDB("record.xml")
      RecordHelper.insertCallData("123456", formatter.parseDateTime("2014-12-01 08:35:12").toDate, None)
      Calls.callExistsAfterTime(formatter.parseDateTime("2014-12-01 01:00:00").toDate) should beTrue
      Calls.callExistsAfterTime(formatter.parseDateTime("2014-12-01 09:00:00").toDate) should beFalse
    }
  }
}
