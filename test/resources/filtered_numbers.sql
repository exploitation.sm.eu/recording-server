DROP TABLE IF EXISTS filtered_numbers CASCADE;
DROP TYPE IF EXISTS number_type;

CREATE TYPE number_type AS ENUM ('incall', 'internal_number');

CREATE TABLE filtered_numbers (
    id SERIAL PRIMARY KEY,
    number VARCHAR(40) NOT NULL,
    type number_type NOT NULL,
    UNIQUE (number, type)
);