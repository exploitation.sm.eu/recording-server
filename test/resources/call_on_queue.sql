DROP TABLE IF EXISTS "call_on_queue" CASCADE;
DROP TYPE IF EXISTS "call_exit_type" CASCADE;

CREATE TYPE "call_exit_type" AS ENUM (
  'full',
  'closed',
  'joinempty',
  'leaveempty',
  'divert_ca_ratio',
  'divert_waittime',
  'answered',
  'abandoned',
  'timeout'
);

CREATE TABLE "call_on_queue" (
 "id" SERIAL PRIMARY KEY,
 "callid" VARCHAR(32) NOT NULL,
 "time" timestamp NOT NULL,
 "ringtime" INTEGER NOT NULL DEFAULT 0,
 "talktime" INTEGER NOT NULL DEFAULT 0,
 "waittime" INTEGER NOT NULL DEFAULT 0,
 "status" call_exit_type NOT NULL,
 "queue_ref" VARCHAR(50),
 "agent_num" VARCHAR(50)
);

CREATE INDEX "call_on_queue__idx__callid" ON call_on_queue(callid);
