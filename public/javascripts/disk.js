angular.module('Disk', ['angularCharts'])
.controller('DiskController', function($scope) {

    $scope.processJson = function(json) {
        $scope.partitions = json;
        for(var i=0; i<json.length; i++) {
            var p = $scope.partitions[i];
            p.chartData = {
                series: ['GB'],
                data: [{
                    x: 'Libre',
                    y: [(p.freeSpace/1000000000).toFixed(2)],
                    tooltip: (p.freeSpace/1000000000).toFixed(2) + ' GB'
                }, {
                    x: 'Utilisé',
                    y: [((p.totalSpace - p.freeSpace)/1000000000).toFixed(2)],
                    tooltip: ((p.totalSpace - p.freeSpace)/1000000000).toFixed(2) + ' GB'
                }]
            };
            p.chartConfig = {
                title : p.name + ' (' + p.mountPoint + ')',
                colors: ['blue', 'red'],
                legend: {
                    display: true,
                    position: 'right'
                }
            };
        }
        $scope.$apply();
    }

    $.ajax({
        type: 'GET',
        url: '/partitions'
    }).then($scope.processJson);
})