function SdaController($scope) {
    function getSdas() {
        $.ajax({
            type : 'GET',
            url : '/filtered_incalls'
        }).then(function(json) {
            $scope.sdas = json;
            $scope.error = null;
            $scope.$apply();
        });
    }

    function displayError(response) {
        $scope.error = response.responseJSON;
        $scope.$apply();
    }

    $scope.deleteSda = function(sda) {
        $.ajax({
            type : 'DELETE',
            url : '/filtered_incalls/' + sda
        }).then(getSdas);
    }



    $scope.createSda = function() {
        $.ajax({
            type : 'POST',
            url : '/filtered_incalls',
            contentType: 'application/json',
            data: JSON.stringify($scope.newSda),
            success: getSdas,
            error: displayError
        });
    }

    getSdas();
}

function NumberController($scope) {
    function getNumbers() {
        $.ajax({
            type : 'GET',
            url : '/filtered_internal_numbers'
        }).then(function(json) {
            $scope.numbers = json;
            $scope.error = null;
            $scope.$apply();
        });
    }

    function displayError(response) {
        $scope.error = response.responseJSON;
        $scope.$apply();
    }

    $scope.deleteNumber = function(number) {
        $.ajax({
            type : 'DELETE',
            url : '/filtered_internal_numbers/' + number
        }).then(getNumbers);
    }



    $scope.createNumber = function() {
        $.ajax({
            type : 'POST',
            url : '/filtered_internal_numbers',
            contentType: 'application/json',
            data: JSON.stringify($scope.newNumber),
            success: getNumbers,
            error: displayError
        });
    }

    getNumbers();
}
