var PAGE_SIZE = 50;

$(document).ajaxStart(function() {
    $('#loading').show()
}).ajaxStop(function() {
    $('#loading').hide()
})

function PageController($scope) {

    $scope.find = find;
    $scope.direction = 'all';

    $scope.processJson = function(json) {
        $scope.records = json.records;
        $scope.hasNext = json.hasNext;
        $scope.$apply()
    }

    $('#start').datetimepicker({
      language: 'fr',
      defaultDate: moment().subtract(1, 'd')
    });

    $('#end').datetimepicker({
        language: 'fr'
    });

    $('#loading').hide();
    find(1);

    function find(pageNum) {
        $scope.currentPage = pageNum
        $scope.fetchFunction = $scope.find
        var request = {
            key: 'recording'
        }
        if ($scope.callee)
            request.callee = $scope.callee;
        if ($scope.caller)
            request.caller = $scope.caller;
        if ($scope.agent)
            request.agent = $scope.agent;
        if ($scope.queue)
            request.queue = $scope.queue;
        if($('#start').val() != '')
            request.start = formatDate($('#start').data("DateTimePicker").getDate());
        if($('#end').val() != '')
            request.end= formatDate($('#end').data("DateTimePicker").getDate());
        if($scope.incoming && $scope.outgoing)
            request.direction = 'all';
        request.direction = $scope.direction;

        if($scope.attached_key && $scope.attached_value){
            request.attached_key = $scope.attached_key;
            request.attached_value = $scope.attached_value;
        }

        $.ajax({
            type : "POST",
            url : window.location.origin + '/records/search?page=' + pageNum + '&pageSize=' + PAGE_SIZE,
            contentType : 'application/json',
            data : JSON.stringify(request)
        }).then($scope.processJson);
    }

    function formatDate(date) {
        return date.format('YYYY-MM-DD HH:mm:ss');
    }

    $scope.searchByCallid = function () {
        $.ajax({
            type : "POST",
            url : window.location.origin + '/records/callid_search?callid=' + $scope.callid
        }).then($scope.processJson);
    }
}
