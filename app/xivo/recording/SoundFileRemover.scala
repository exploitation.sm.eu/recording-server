package xivo.recording

import java.io.File

import models.{Calls, Record}
import org.joda.time.{DateTime, Period}
import play.Application
import play.api.db.DB

class SoundFileRemover(app: Application) extends Runnable {

  val audioDir = app.configuration().getString("audio.folder")

  def recentCallExists(): Boolean = {
    implicit val realApp = app.getWrappedApplication
    DB.withConnection("stats")(implicit c =>
      Calls.callExistsAfterTime(new DateTime().minusDays(1).toDate))
  }

  override def run(): Unit = {
    if(recentCallExists())
      dirVisitor(new File(audioDir), fileAction(_))
  }

  def dirVisitor(dir: File, action: File => Unit): Unit = {
    for(file <- dir.listFiles()) {
      if(file.isDirectory)
        dirVisitor(file, action)
      else
        action(file)
    }
  }

  def fileAction(file: File): Unit = {
    val fileAge = new Period(new DateTime(file.lastModified()), new DateTime())
    if(fileAge.getDays == 1) {
      val globalId = file.getName.reverse.substring(file.getName.reverse.indexOf('.') + 1, file.getName.length).reverse
      DB.withConnection("stats")(implicit c => {
        if(!Record.globalIdExists(globalId))
          file.delete()
      })(app.getWrappedApplication)
    }

  }
}
