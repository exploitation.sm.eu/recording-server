package xivo.recording

import java.util.concurrent.TimeUnit

import play.libs.Akka
import play.{GlobalSettings, Application}

import scala.concurrent.duration.FiniteDuration

class Main extends GlobalSettings {

  override def onStart(app: Application) = {
    val phase = FiniteDuration(1, TimeUnit.DAYS)
    val frequency = FiniteDuration(1, TimeUnit.DAYS)
    Akka.system().scheduler.schedule(phase, frequency, new SoundFileRemover(app))(Akka.system().dispatcher)
  }
}
