package controllers

import models.User
import models.authentication.{TeacherRight, RightsHelper}
import play.api._
import play.api.data.Form
import play.api.data.Forms._
import play.api.mvc._
import views.html

object Login extends Controller {

  val logger = Logger(getClass.getName)

  def login = Action { implicit request => Ok(html.login(loginForm)) }

  val loginForm = Form(
    tuple(
      "login" -> text,
      "mdp" -> text) verifying ("Login ou mot de passe invalide", result => result match {
        case (login, mdp) => User.authenticate(login, mdp).isDefined
      }))

  def authenticate = Action { implicit request =>
    loginForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.login(formWithErrors)),
      user => RightsHelper.getFreshUserRights(user._1) match {
        case None => BadRequest(html.login(loginForm.withGlobalError("Aucun droit d'accès n'existe pour vous")))
        case Some(TeacherRight(_, _, _, start, end)) if end.isBeforeNow || start.isAfterNow =>
          BadRequest(html.login(loginForm.withGlobalError("Vos droits d'accès ne sont pas valides pour la période actuelle")))
        case _ => Redirect(routes.Application.index()).withSession("username" -> user._1)
      }
    )
  }

  def logout = Action { implicit request =>
    logger.info(s"L'utilisateur ${request.session.get("username")} s'est déconnecté")
    Redirect(routes.Login.login).withNewSession
  }
}