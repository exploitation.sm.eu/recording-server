package controllers

import models.{FilteredInternalNumbers, NumberAlreadyExistsException, FilteredIncalls}
import play.api.db.DB
import play.api.libs.json.{JsString, Json}
import play.api.mvc.{Action, Call, Controller}
import play.api.Play.current

object RecordingControl extends Controller with Secured with RightsCheck {

  override val loginRoute: Call = routes.Login.login()

  def page() = IsAuthenticated(true, implicit request => {
    if(isAdmin)
      Ok(views.html.recording_control())
    else
      Forbidden(views.html.forbidden())
  })

  def allFilteredIncalls() = IsAuthenticated(false, implicit request => {
    if(isAdmin())
      DB.withConnection(implicit c => Ok(Json.toJson(FilteredIncalls.all())))
    else
      Forbidden
  })

  def getFilteredIncall(number: String) = Action {
    implicit request => DB.withConnection(implicit c => FilteredIncalls.find(number) match {
      case None => NotFound
      case Some(number) => Ok(Json.toJson(number))
    })
  }

  def deleteFilteredIncall(number: String) = IsAuthenticated(false, implicit request => {
    if(isAdmin()) {
      DB.withConnection(implicit c => FilteredIncalls.delete(number))
      NoContent
    } else  Forbidden
  })

  def createFilteredIncall = IsAuthenticated(false, implicit request => {
    if(isAdmin()) {
      request.body.asJson match {
        case Some(JsString(number)) => try {
          DB.withConnection(implicit c => FilteredIncalls.create(number))
          Created
        } catch {
          case e: NumberAlreadyExistsException => BadRequest(Json.toJson(s"Le numéro $number existe déjà"))
        }
        case _ => BadRequest
      }
    } else  Forbidden
  })

  def allFilteredInternalNumbers = IsAuthenticated(false, implicit request => {
    if(isAdmin())
      DB.withConnection(implicit c => Ok(Json.toJson(FilteredInternalNumbers.all())))
    else
      Forbidden
  })

  def getFilteredInternalNumber(num: String) = Action {
    implicit request => DB.withConnection(implicit c => FilteredInternalNumbers.find(num) match {
      case None => NotFound
      case Some(number) => Ok(Json.toJson(number))
    })
  }

  def createFilteredInternalNumber = IsAuthenticated(false, implicit request => {
    if(isAdmin()) {
      request.body.asJson match {
        case Some(JsString(number)) => try {
          DB.withConnection(implicit c => FilteredInternalNumbers.create(number))
          Created
        } catch {
          case e: NumberAlreadyExistsException => BadRequest(Json.toJson(s"Le numéro $number existe déjà"))
        }
        case _ => BadRequest
      }
    } else  Forbidden
  })

  def deleteFilteredInternalNumber(number: String) = IsAuthenticated(false, implicit request => {
    if(isAdmin()) {
      DB.withConnection(implicit c => FilteredInternalNumbers.delete(number))
      NoContent
    } else  Forbidden
  })
}
