package controllers

import java.io.File

import models.Record
import play.api._
import play.api.db.DB
import play.api.mvc._
import play.api.Play.current

import scala.collection.JavaConverters._

object Files extends Controller with Secured {

  val audioFolder = Play.current.configuration.getString("audio.folder").get
  val audioExtensions = Play.current.configuration.getStringList("audio.extensions").get
  val logger = Logger(getClass.getName)

  def get(globalId: String) = IsAuthenticated(true, implicit request => {
    val file = new File(globalId).getName()
    val gwName = file.split("-")(0)
    DB.withConnection("stats") { implicit c =>
      logger.info( s"""${request.session.get("username").getOrElse("unknown")} a demandé le fichier le fichier $audioFolder/$gwName/$file.[OneOfConfiguredExtensions],
         correspondant à l'appel ${Record.callidByGlobalId(globalId).getOrElse("unknown")}""")
    }

    val extension = audioExtensions.asScala.filter(audioExtension => new File(s"$audioFolder/$gwName/$file.$audioExtension").exists())

    if(extension.length > 0) {
      val realFile = new File(s"$audioFolder/$gwName/$file.${extension.head}")
      Ok.sendFile(realFile)
    }
    else {
      logger.warn(s"Fichier $audioFolder/$gwName/$file.[$audioExtensions] non trouvé")
      NotFound(views.html.nosuchfile())
    }
  })

  override val loginRoute: Call = routes.Login.login()
}
