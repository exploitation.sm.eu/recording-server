package controllers

import models.CallDetail
import org.joda.time.format.PeriodFormatterBuilder
import play.api.Logger
import play.api.Play.current
import play.api.db.DB
import play.api.libs.json.Json
import play.api.mvc.{Action, Controller}

object History extends Controller {
  val format = "yyyy-MM-dd HH:mm:ss"
  val periodFormat = new PeriodFormatterBuilder()
    .minimumPrintedDigits(2).printZeroAlways().appendHours().appendSeparator(":").appendMinutes().appendSeparator(":").appendSeconds().toFormatter

  val logger = Logger(getClass.getName)

  def search(size: Int) = Action(implicit request => {
    request.body.asJson.flatMap(value => (value \ "interface").asOpt[String]) match {
      case None => BadRequest(Json.toJson("Missing required parameter interface"))
      case Some(interface) => DB.withConnection("stats")(
        implicit c => Ok(Json.toJson(CallDetail.historyByInterface(interface, size))))
    }
  })
}
