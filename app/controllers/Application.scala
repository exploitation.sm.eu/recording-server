package controllers

import models.authentication.RightsHelper
import models.{Paginator, Record, SearchCriteria}
import play.api.Play.current
import play.api._
import play.api.db.DB
import play.api.mvc._

object Application extends Controller with Secured {

  override val loginRoute: Call = routes.Login.login()



  val logger = Logger(getClass.getName)

  def index() = IsAuthenticated(true, implicit request => Ok(views.html.index()))

  def search(page: Int, pageSize: Int) = IsAuthenticated(false, implicit request => {
    DB.withConnection("stats")(implicit connection => {
      val offset = (page - 1) * pageSize
      implicit val paginator = Paginator(offset, pageSize + 1)
      val query = request.body.asJson
      logger.info(s"Requête : $query pour la page $page")
      val criteria = SearchCriteria.fromJson(query.get)
      val right = RightsHelper.getUserRights(request.session.get("username").get).get
      val records = Record.search(criteria, right)
      //logger.info(s"Requête : ${Record.sSql}")
      Ok(PaginationHelper.paginateAndJsonify(records, pageSize))
    })
  })

  def callidSearch(callid: String) = IsAuthenticated(false, implicit request => {
    DB.withConnection("stats")(implicit c => {
      implicit val paginator = Paginator(0, 1)
      val right = RightsHelper.getUserRights(request.session.get("username").get).get
      val records = Record.searchByCallid(callid, right)
      Ok(PaginationHelper.paginateAndJsonify(records, 1))
    })
  })


}