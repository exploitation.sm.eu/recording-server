package controllers

import models.authentication.{AdminRight, RightsHelper}
import play.api.mvc.Request

trait RightsCheck {

  def isAdmin()(implicit r: Request[_]) = r.session.get("username") match {
    case None => false
    case Some(username) => RightsHelper.getUserRights(username).equals(Some(AdminRight()))
  }
}
