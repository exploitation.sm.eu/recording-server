package controllers

import models.Partition
import play.api.libs.json.Json
import play.api.mvc.{Call, Controller}

object Disk extends Controller with Secured with RightsCheck {

  def page = IsAuthenticated(true, implicit request => {
    if(isAdmin)
      Ok(views.html.disk())
    else
      Forbidden(views.html.forbidden())
  })

  def listPartitions = IsAuthenticated(false, implicit request => {
    if(isAdmin)
      Ok(Json.toJson(Partition.all.map(p => Json.obj(
        "name" -> p.name,
        "mountPoint" -> p.mountPoint,
        "freeSpace" -> p.freeSpace,
        "totalSpace" -> p.totalSpace
      ))))
    else
      Forbidden
  })

  override val loginRoute: Call = routes.Login.login()
}
