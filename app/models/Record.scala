package models

import java.sql.Connection
import java.util.Date

import anorm.SqlParser._
import anorm._
import org.joda.time.{DateTime, Duration}

case class Record(callDataId: Int,
                  start: DateTime,
                  duration: Option[Duration],
                  srcNum: String,
                  dstNum: String,
                  agent: String,
                  queue: String,
                  attachedData: Map[String, String]=Map()) {
  val recordId: Option[String] = attachedData.get("recording")
}

object Record {
  var sSql:String="";
  def simple(implicit c: Connection) = get[Int]("call_data_id") ~
  get[Date]("start_time") ~
  get[Option[Date]]("end_time") ~
  get[Option[String]]("src_num") ~
  get[Option[String]]("dst_num") ~
  get[Option[String]]("agent_num") ~
  get[Option[String]]("firstname") ~
  get[Option[String]]("lastname") ~
  get[Option[String]]("queue_ref") ~
  get[Option[String]]("queue_number") map {
    case callDataId ~ start ~ end ~ srcNum ~ dstNum ~ agentNum ~ agentFirstname ~ agentLastname ~ queue ~ queueNum => {
      Record(
        callDataId,
        new DateTime(start),
        end.map(d => new Duration(new DateTime(start), new DateTime(d))),
        srcNum.getOrElse(""), dstNum.getOrElse(""),
        agentNum.map(num => s"${agentFirstname.getOrElse("")} ${agentLastname.getOrElse("")} ($num)").getOrElse(""),
        queue.map(name => s"$name (${queueNum.getOrElse("")})").getOrElse(""),
        getAttachedData(callDataId)
      )
    }
  }

  def simpleAttachedData = get[String]("key") ~ get[String]("value") map {
    case key ~ value => key -> value
  }

  private def getAttachedData(callDataId: Int)(implicit c: Connection): Map[String, String] =
    SQL("SELECT key, value FROM attached_data WHERE id_call_data = {callDataId}").on('callDataId -> callDataId).as(simpleAttachedData *).toMap

  def search(criteria: SearchCriteria, right: models.authentication.Right)(implicit connection: Connection, paginator: Paginator): List[Record] ={
    this.sSql = s"""SELECT DISTINCT ON(start_time, cd.id) cd.id AS call_data_id, start_time, end_time, src_num, dst_num,
        COALESCE(cq.agent_num, src_agent, dst_agent) AS agent_num, queue_ref,
        firstname, lastname, q.number AS queue_number
                FROM call_data cd
                LEFT JOIN attached_data ad ON ad.id_call_data = cd.id
                LEFT JOIN attached_data ac ON ac.id_call_data = cd.id
                LEFT JOIN call_on_queue cq ON cq.callid = cd.uniqueid
                LEFT JOIN agentfeatures a ON COALESCE(cq.agent_num, src_agent, dst_agent) = a.number
                LEFT JOIN queuefeatures q ON cq.queue_ref = q.name
                WHERE (${criteria.buildWhereClause()}) AND (${right.buildWhereClause()})
                ORDER BY start_time DESC, cd.id DESC
                OFFSET {offset} LIMIT {limit}"""
    SQL(this.sSql)
      .on(criteria.buildParams(): _*).on('offset -> paginator.offset, 'limit -> paginator.size).as(Record.simple *)
  }

  def searchByCallid(callid: String, right: models.authentication.Right)(implicit connection: Connection, paginator: Paginator): List[Record] =
    search(CallidSearchCriteria(callid), right)

  def globalIdExists(globalId: String)(implicit c: Connection): Boolean =
    SQL("SELECT id FROM attached_data ad WHERE ad.key = \'recording\' AND ad.value = {globalId}").on('globalId -> globalId)
      .executeQuery().resultSet.next()

  def callidByGlobalId(globalId: String)(implicit c: Connection): Option[String] =
    SQL("SELECT cd.uniqueid FROM call_data cd JOIN attached_data ad ON cd.id = ad.id_call_data WHERE ad.key = 'recording' AND ad.value = {globalId}")
      .on('globalId -> globalId).as(get[String]("uniqueid") *).headOption
}

