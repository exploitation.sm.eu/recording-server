package models

import java.io.File

import scala.io.Source

case class Partition(name: String, mountPoint: String, totalSpace: Long, freeSpace: Long)

object Partition {
  def all: List[Partition] = {
    val lines = Source.fromFile("/proc/mounts").getLines()
    (for(line <- lines if line.startsWith("/dev")) yield {
      val fields = line.split(" ")
      val partition = fields(0)
      val mountPoint = fields(1)
      val file = new File(mountPoint)
      val diskTotal = file.getTotalSpace
      val diskFree = file.getFreeSpace
      Partition(partition, mountPoint, diskTotal, diskFree)
    }).toList
  }
}