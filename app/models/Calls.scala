package models

import java.util.Date
import java.sql.Connection
import anorm.SQL

object Calls {

  def callExistsAfterTime(time: Date)(implicit c: Connection): Boolean =
    SQL("SELECT start_time FROM call_data WHERE start_time >= {time}").on('time -> time).singleOpt.isDefined
}
