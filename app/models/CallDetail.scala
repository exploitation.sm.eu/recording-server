package models

import java.sql.Connection
import java.util.Date

import anorm.SqlParser._
import anorm._
import org.joda.time.format.{PeriodFormatterBuilder, DateTimeFormat}
import org.joda.time.{DateTime, Duration}
import play.api.libs.json.{Json, Writes}

case class CallDetail(start: DateTime,
                      duration: Option[Duration],
                      srcNum: Option[String],
                      dstNum: Option[String],
                      status: CallStatus.CallStatus)

object CallStatus extends Enumeration {
  type CallStatus = Value
  val Emitted = Value("emitted")
  val Answered = Value("answered")
  val Missed = Value("missed")
  val Ongoing = Value("ongoing")
}

object CallDetail {

  val format = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
  val periodFormat = new PeriodFormatterBuilder()
    .minimumPrintedDigits(2).printZeroAlways().appendHours().appendSeparator(":").appendMinutes().appendSeparator(":").appendSeconds().toFormatter

  val simple = get[Date]("start_time") ~
    get[Option[Date]]("end_time") ~
    get[Option[String]]("src_num") ~
    get[Option[String]]("dst_num") ~
    get[String]("status") map {
      case start ~ end ~ src ~ dst ~ status =>
        CallDetail(new DateTime(start), end.map(d => new Duration(new DateTime(start), new DateTime(d))), src, dst, CallStatus.withName(status))
    }

  def historyByInterface(interface: String, size: Int)(implicit connection: Connection): List[CallDetail] =
    SQL("""SELECT call_data_id, start_time, end_time, src_num, dst_num, status FROM (
           (SELECT id AS call_data_id, start_time, end_time, src_num, dst_num, 'emitted' AS status
            FROM call_data WHERE src_interface = {interface} ORDER BY call_data_id DESC LIMIT {limit})
           UNION
           (SELECT cd.id AS call_data_id, ce.start_time, ce.end_time, src_num, dst_num,
                   CASE WHEN ce.answer_time IS NULL AND ce.end_time IS NULL THEN 'ongoing'
                        WHEN ce.answer_time IS NULL AND ce.end_time IS NOT NULL THEN 'missed'
                        ELSE 'answered' END
            FROM call_data cd JOIN call_element ce ON cd.id = ce.call_data_id
            WHERE ce.interface = {interface} ORDER BY call_data_id DESC LIMIT {limit})) t
            ORDER BY call_data_id DESC LIMIT {limit}""")
      .on('limit -> size, 'interface -> interface).as(simple *)

  implicit val writes = new Writes[CallDetail] {
    def writes(call: CallDetail) = Json.obj(
      "start" -> call.start.toString(format),
      "duration" -> call.duration.map(d => periodFormat.print(d.toPeriod)),
      "src_num" -> call.srcNum,
      "dst_num" -> call.dstNum,
      "status" -> call.status.toString
    )
  }
}