package models.authentication

import play.api.Play
import play.api.libs.ws.WS
import play.api.Play.current
import scala.concurrent.Await
import scala.concurrent.duration._
import play.api.cache.Cache

object RightsHelper {

  def WsAddress = Play.configuration.getString("rightsManagement.address").get

  def getUserRights(username: String): Option[Right] = Cache.get(username) match {
    case None => getFreshUserRights(username)
    case Some(right) => Some(right.asInstanceOf[Right])
  }

  def getFreshUserRights(username: String): Option[Right] = getUserRightFromWebService(username) match {
    case None => None
    case Some(right) => Cache.set(username, right)
      Some(right)
  }

  private def getUserRightFromWebService(username: String): Option[Right] = {
    val res = Await.result(WS.url(s"http://$WsAddress/rights/user/$username").get(), 5.second)
    if (res.status != 200)
      return None
    Some(Right.fromJson(res.json))
  }

}
