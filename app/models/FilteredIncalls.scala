package models

import java.sql.Connection
import anorm.SQL
import anorm.SqlParser._

object FilteredIncalls {

  val simpleString = get[String]("number")

  def delete(number: String)(implicit c: Connection) = SQL("DELETE FROM filtered_numbers WHERE number = {number} AND type = 'incall'::number_type")
    .on('number -> number).executeUpdate()

  def all()(implicit c: Connection): List[String] = SQL("SELECT number FROM filtered_numbers WHERE type = 'incall'::number_type")
    .as(simpleString *).sorted

  def create(number: String)(implicit c: Connection) = SQL("SELECT number FROM filtered_numbers WHERE number = {number} AND type = 'incall'::number_type")
    .on('number -> number).singleOpt() match {
    case None => SQL("INSERT INTO filtered_numbers(number, type) VALUES ({number}, 'incall'::number_type)").on('number -> number).executeUpdate()
    case Some(_) => throw new NumberAlreadyExistsException(number)
  }

  def find(number: String)(implicit c: Connection): Option[String] =
    SQL("SELECT number FROM filtered_numbers WHERE number = {number} AND type = 'incall'::number_type").on('number -> number).as(simpleString *).headOption

}
