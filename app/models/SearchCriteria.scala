package models

import java.lang.reflect.Field
import java.sql.Connection

import anorm.{ParameterValue, NamedParameter}
import models.CallDirection.CallDirection
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import play.api.libs.json.JsValue

case class QueryField(parameterName: String, filter: String)

abstract class SearchCriteria {

  val queryFields: Map[Field, QueryField]

  implicit def anyToParameterValue(obj: Any): ParameterValue = obj match {
    case Some(o) => anyToParameterValue(o)
    case d: DateTime => ParameterValue.toParameterValue(d.toDate)
    case i: Int => ParameterValue.toParameterValue(i)
    case t => ParameterValue.toParameterValue(t.toString)
  }

  def getCriteriaField(fieldName: String): Field = {
    val field = getClass().getDeclaredField(fieldName)
    field.setAccessible(true)
    field
  }

  def buildWhereClause()(implicit c: Connection): String

  def buildParams(): List[NamedParameter] = queryFields.filter(t => t._1.get(this) != None)
    .map(t => NamedParameter(t._2.parameterName, t._1.get(this)))(collection.breakOut)
}

case class GenericSearchCriteria(callee: Option[String], caller: Option[String], agent: Option[String], queue: Option[String],
                          start: Option[DateTime], end: Option[DateTime], direction: CallDirection, key: Option[String],
                          attached_key: Option[String],attached_value: Option[String]) extends SearchCriteria {
  override val queryFields = Map(
    getCriteriaField("callee") -> QueryField("callee", "dst_num = {callee}"),
    getCriteriaField("caller") -> QueryField("caller", "src_num = {caller}"),
    getCriteriaField("start")  -> QueryField("start", "({start} <= start_time OR {start} <= end_time)"),
    getCriteriaField("end")    -> QueryField("end", "{end} >= start_time"),
    getCriteriaField("key")    -> QueryField("key", "ad.key = {key}"),
    getCriteriaField("attached_key")   -> QueryField("attached_key", "ac.key = {attached_key}"),
    getCriteriaField("attached_value") -> QueryField("attached_value", "ac.value = {attached_value}"))

  override def buildWhereClause()(implicit c: Connection): String = {


    var res = "end_time IS NOT NULL"
    for((objectField, queryField) <- queryFields) {
      if(objectField.get(this) != None)
        res += s" AND ${queryField.filter}"
    }
    if(direction == CallDirection.Incoming)
      res += " AND call_direction = 'incoming'::call_direction_type"
    else if(direction == CallDirection.Outgoing)
      res += " AND call_direction = 'outgoing'::call_direction_type"
    if(queue.isDefined) {
      val queueNames = queue.get::XivoObjects.queueNamesForPattern(queue.get)
      res += s" AND ${SqlUtils.createInClauseOrTrue("cq.queue_ref", queueNames)}"
    }
    if(agent.isDefined) {
      val agentNumbers = agent.get::XivoObjects.agentsNumbersForPattern(agent.get)
      res +=
        s""" AND (${SqlUtils.createInClauseOrTrue("cq.agent_num", agentNumbers)}
           OR ${SqlUtils.createInClauseOrTrue("cd.src_agent", agentNumbers)}
           OR ${SqlUtils.createInClauseOrTrue("cd.dst_agent", agentNumbers)})"""
    }
    res
  }
}

case class CallidSearchCriteria(callid: String) extends SearchCriteria {
  override val queryFields = Map(getCriteriaField("callid")  -> QueryField("callid", "uniqueid = {callid}"))

  override def buildWhereClause()(implicit c: Connection): String = "uniqueid = {callid}"
}

object SearchCriteria {
  val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")

  def fromJson(json: JsValue): SearchCriteria = {
    def extractDate(dateOpt: Option[String]): Option[DateTime] = dateOpt match {
      case None => None
      case Some(date) => try {
        Some(DateTime.parse(date, formatter))
      } catch {
        case e: IllegalArgumentException => None
      }
    }
    GenericSearchCriteria((json \ "callee").asOpt[String], (json \ "caller").asOpt[String], (json \ "agent").asOpt[String],
      (json \ "queue").asOpt[String], extractDate((json \ "start").asOpt[String]), extractDate((json \ "end").asOpt[String]),
      CallDirection.withName((json \ "direction").asOpt[String].getOrElse("all")), (json \ "key").asOpt[String]
    , (json \ "attached_key").asOpt[String], (json \ "attached_value").asOpt[String])
  }
}

object CallDirection extends Enumeration {
  type CallDirection = Value
  val Incoming = Value("incoming")
  val Outgoing = Value("outgoing")
  val All = Value("all")
}