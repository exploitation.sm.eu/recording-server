package models

object SqlUtils {

  def createInClauseOrTrue(column: String, values: List[String]): String = createInClauseOrOption(column, values, "TRUE")

  private def createInClauseOrOption(column: String, values: List[String], replacement: String): String = {
    var clause = ""
    if(values.size == 0)
      clause = replacement
    else {
      clause = s"$column IN ("
      values.foreach(cid => clause += s"'$cid',")
      clause = clause.substring(0, clause.length - 1)
      clause += ")"
    }
    clause
  }

  def createInClauseOrFalse(column: String, values: List[String]) = createInClauseOrOption(column, values, "FALSE")

}
